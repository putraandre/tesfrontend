const pkg = require('./package')

module.exports = {
  mode: 'universal',

  /*
  ** Headers of the page
  */
  head: {
    title: pkg.name,
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: pkg.description }
    ],
    /* script: [
      { src: 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js' },
      { src: 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/popper.min.js' },
      { src: 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/bootstrap.min.js' }
    ], */
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },/* 
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Roboto' },
      { rel: 'stylesheet', href: '/static/bootstrap.min.css' },
      { rel: 'stylesheet', href: '/static/mdb.min.css' },
      { rel: 'stylesheet', href: '/static/all.css' } */
    ],
    css: [
        { src: '../static/bootstrap.min.css',},
        { src: '../static/bootstrap.min.css',},
        { src: '../static/bootstrap.min.css',}
    ],
    build: {
            extractCSS: true
        }
  },

  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },

  /*
  ** Global CSS
  */
  css: [
  ],

  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
  ],

  /*
  ** Nuxt.js modules
  */
 
  

modules: [,
    // Doc: https://bootstrap-vue.js.org/docs/
    'bootstrap-vue/nuxt'
  ], 
  

  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {
      
    }
  }
}
